using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerManager : MonoBehaviour
{

    [SerializeField] private int Power;
    [SerializeField] private float Counter, counterReset = 20;

    // Start is called before the first frame update
    void Start()
    {
        Power = 100;
    }

    // Update is called once per frame
    void Update()
    {
        Power = Mathf.Clamp(Power, 0, 100);
        if (Power == 1)
        {
            if (Counter <= 0)
            {

            }
        }
        else
        {
            Counter = counterReset;
        }

        Counter -= Time.deltaTime;
    }

    public int GetPower()
    {
        return Power;
    }
    public void SetPower(int value)
    {
        this.Power = value;
    }
}
