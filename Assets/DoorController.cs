using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    [SerializeField] private Vector3 BasePos, DestPos;
    [SerializeField] private bool IsOpen = true;
    [SerializeField] private float Speed;

    // Start is called before the first frame update
    void Start()
    {
        IsOpen = true;
        BasePos = transform.localPosition;
        DestPos = BasePos - new Vector3(0, 4, 0);

    }

    // Update is called once per frame
    void Update()
    {
            if (IsOpen)
            {
                transform.localPosition = Vector3.Lerp(transform.localPosition, BasePos, Speed * Time.deltaTime);
            }
            else
            {
                transform.localPosition = Vector3.Lerp(transform.localPosition, DestPos, Speed * Time.deltaTime);
            }


    }


    public void SetIsOpen(bool value)
    {
        IsOpen = value;
    }
    public bool GetIsOpen()
    {
        return IsOpen;
    }
}
