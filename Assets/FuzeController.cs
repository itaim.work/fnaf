using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuzeController : MonoBehaviour
{
    [SerializeField] private Transform Player;
    [SerializeField] private float Distance, maxDist;
    [SerializeField] private PowerManager PowerManagerScript;
    RaycastHit hit;


    // Start is called before the first frame update
    void Start()
    {
        PowerManagerScript = GameObject.Find("PowerManager").GetComponent<PowerManager>();
        Player = GameObject.Find("Player").transform;

    }

    // Update is called once per frame
    void Update()
    {

        Distance = Vector3.Distance(transform.position, Player.position);

        if (Distance <= maxDist)
        {
            if (Physics.Raycast(Player.position, Camera.main.transform.forward, out hit, maxDist))
            {
                if (hit.transform.CompareTag("Fuze"))
                {
                    print("Press [E] to get 33 power");


                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        PowerManagerScript.SetPower(PowerManagerScript.GetPower() + 33);
                    }
                }
            }
        }
    }
}