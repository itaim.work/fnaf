using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float MouseSensitivity = 100f; // sets the sensitivity of the mouse to move
    [SerializeField] private Transform PlayerBody; //creates reference to a public playerbody
    float XRotation = 0f; // set rotation 

    [SerializeField] private PauseMenu PauseMenuScript;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked; //lock the mouse the center of the screen
    }

    // Update is called once per frame
    void Update()
    {

        if (!PauseMenuScript.GetIsPaused())
        {
            float MouseX = Input.GetAxis("Mouse X") * MouseSensitivity * Time.deltaTime; // speed of movement of the mouse from left to right
            float MouseY = Input.GetAxis("Mouse Y") * MouseSensitivity * Time.deltaTime; // speed of movement of the mouse from up to down
            XRotation -= MouseY; // updates he rotation
            XRotation = Mathf.Clamp(XRotation, -90f, 90f); // makes that the player cant see backwards by moving in y dimention
            transform.localRotation = Quaternion.Euler(XRotation, 0f, 0f); // returns the function in the mindset f angles
            PlayerBody.Rotate(Vector3.up * MouseX); // spins the camera

        }

    }
}
