using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class HunterAgentController : AgentController
{
    private RaycastHit hit;

    [SerializeField] private int maxAngle;
    [SerializeField] private float maxRadius, CounterReset;
    private float angle, counter;
    private Vector3 DirectionTowards;


    [SerializeField] private bool isInFOV = false;

    // Start is called before the first frame update
    void Start()
    {
        Agent = gameObject.GetComponent<NavMeshAgent>();

        target = GameObject.Find("Player").transform;

        WayPoints = GameObject.FindGameObjectsWithTag("WayPoint");

    }

    // Update is called once per frame
    void Update()
    {
        if (counter <= 0 && isInFOV)
        {
            isInFOV = false;
            Agent.SetDestination(WayPoints[Random.Range(0, WayPoints.Length)].transform.position);
        }
        if (Agent.remainingDistance <= 0.5f && !isInFOV)
        {
            Agent.SetDestination(WayPoints[Random.Range(0, WayPoints.Length)].transform.position);
        }
        if (isInFOV)
        {
            Agent.SetDestination(target.position);
        }

        DirectionTowards = (target.position - transform.position).normalized;

        if (Physics.Raycast(transform.position, DirectionTowards, out hit, maxRadius))
        {

            if (hit.transform.CompareTag("Player"))
            {
                angle = Vector3.Angle(transform.forward, DirectionTowards);
                if (angle <= maxAngle)
                {
                    isInFOV = true;
                    counter = CounterReset;
                }
            }

        }
        counter -= Time.deltaTime;
    }

    /// <summary>
    /// visualize the FOV process
    /// </summary>
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;//A color for the Radius
        Gizmos.DrawWireSphere(transform.position, maxRadius);//Draw the radius with a yellow color 

        Vector3 fovLine1 = Quaternion.AngleAxis(maxAngle, transform.up) * transform.forward * maxRadius;//sets the fov aspect, with the radius and the look angel(marked with !)
        Vector3 fovLine2 = Quaternion.AngleAxis(-maxAngle, transform.up) * transform.forward * maxRadius;//!

        Gizmos.color = Color.blue;//sets the FOV lines coloer to blue
        Gizmos.DrawRay(transform.position, fovLine1);//draws the FOV aspect on the screen @
        Gizmos.DrawRay(transform.position, fovLine2);//@

        if (!isInFOV)
            Gizmos.color = Color.red;
        if (isInFOV)
            Gizmos.color = Color.green;//the player position ray color
        Gizmos.DrawRay(transform.position, (target.transform.position - transform.position).normalized * maxRadius);//draws a line to the player position

        Gizmos.color = Color.black;//the forward ray color
        Gizmos.DrawRay(transform.position, transform.forward * maxRadius);//draws a ray to the AI forward direction

    }
}
