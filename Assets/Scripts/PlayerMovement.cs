using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private CharacterController Controller;
    [SerializeField] private int HP = 100, hour = 12;
    //[SerializeField] private UI_Controller_V2 UI_Controller_Script;
    [SerializeField] private float winCounter, WinCounterReset, timeCounter;
    [SerializeField] private Text HourDisplayText;

    Vector3 move;
    public float speed = 5, x, z;


    // Start is called before the first frame update
    void Start()
    {
        HourDisplayText.text = " Time: " + hour + "AM";
        winCounter = WinCounterReset;
        Controller = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.D))
        {
            HP = 0;
        }

        x = Input.GetAxis("Horizontal");
        z = Input.GetAxis("Vertical");

        move = transform.right * x + transform.forward * z;
        move = move * speed * Time.deltaTime;

        Controller.Move(move);

        if (winCounter <= 0)
        {
            print("Player win");
        }

        if (timeCounter >= 45)
        {
            if (hour == 12)
            {
                hour = 1;
            }
            else
            {
                hour++;
            }
            HourDisplayText.text = " Time: " + hour + "AM";
            timeCounter = 0;
        }

        timeCounter += Time.deltaTime;
        winCounter -= Time.deltaTime;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Agent"))
        {
            print("Game lost");
        }
    }
}
