using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BaseAgentController : MonoBehaviour
{
    [SerializeField] protected NavMeshAgent Agent;

    [SerializeField] protected Transform target;

    private void Start()
    {
        Agent = gameObject.GetComponent<NavMeshAgent>();

        target = GameObject.Find("Player").transform;
    }

    private void Update()
    {
        Agent.SetDestination(target.position);
    }


}