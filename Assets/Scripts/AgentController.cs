using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AgentController : BaseAgentController
{

    [SerializeField] protected GameObject[] WayPoints;
    [SerializeField] private float StartCounter = 10;

    // Start is called before the first frame update
    void Start()
    {
        Agent = gameObject.GetComponent<NavMeshAgent>();

        target = GameObject.Find("WayPoint").transform;

        WayPoints = GameObject.FindGameObjectsWithTag("WayPoint");

        Agent.speed = 0;

    }

    // Update is called once per frame
    void Update()
    {
        if(StartCounter <= 0)
        {
            Agent.speed = 5;
        }
        else
        {
            StartCounter -= Time.deltaTime;
        }

        if (Agent.remainingDistance <= 0.5f)
        {
            Agent.SetDestination(WayPoints[Random.Range(0, WayPoints.Length)].transform.position);
        }

    }

}
