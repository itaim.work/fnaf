using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonController : MonoBehaviour
{
    [SerializeField] private Transform Player;
    [SerializeField] private float Distance, maxDist;
    [SerializeField] private DoorController DoorControllerScript;
    [SerializeField] private PowerManager PowerManagerScript;

    RaycastHit hit;
    // Start is called before the first frame update
    void Start()
    {
        PowerManagerScript = GameObject.Find("PowerManager").GetComponent<PowerManager>();
        Player = GameObject.Find("Player").transform;
    }

    // Update is called once per frame
    void Update()
    {

        if (PowerManagerScript.GetPower() > 0)
        {
            Distance = Vector3.Distance(transform.position, Player.position);

            if (Distance <= maxDist)
            {
                if (Physics.Raycast(Player.position, Camera.main.transform.forward, out hit, maxDist))
                {
                    if (hit.transform.CompareTag("Button"))
                    {
                        if (DoorControllerScript.GetIsOpen())
                        {
                            print("press e to close");
                        }
                        else
                        {
                            print("press e to open");
                        }
                        if (Input.GetKeyDown(KeyCode.E) && PowerManagerScript.GetPower() >= 33)
                        {
                            if (DoorControllerScript.GetIsOpen())
                            {
                                PowerManagerScript.SetPower(PowerManagerScript.GetPower() - 33);
                            }
                            DoorControllerScript.SetIsOpen(!DoorControllerScript.GetIsOpen());
                        }
                    }
                }
            }

        }
        else
        {
            DoorControllerScript.SetIsOpen(true);
        }

    }

}
